(defproject nfc-payment-server "0.1.0-SNAPSHOT"
  :description "Payment server for a nfc based payment."
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [cheshire "5.8.1"]
                 [mvxcvi/clj-pgp "0.10.0"]
                 [org.clojure/java.jdbc "0.7.9"]
                 [org.postgresql/postgresql "42.2.5"]
                 [ring/ring-jetty-adapter "1.7.1"]
                 [compojure "1.4.0"]
                 [hiccup "2.0.0-alpha2"]
                 [clj-http "3.10.0"]
                 [clj-time "0.15.1"]
                 [thi.ng/crypto "0.1.0-SNAPSHOT"]
                 [ring/ring-json "0.5.0-beta1"]]
  :plugins [[lein-ring "0.12.5"]]
  :ring {:handler nfc-payment-server.handler/app}
  :repl-options {:init-ns nfc-payment-server.core})
