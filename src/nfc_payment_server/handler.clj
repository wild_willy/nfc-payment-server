(ns nfc-payment-server.handler
  (:require [compojure.core :refer :all]
            [compojure.handler :as handler]
            [nfc-payment-server.controllers.transaction :as transaction]
            [ring.middleware.json :as middleware]
            [compojure.route :as route]))

(defroutes app-routes
  (POST "/transaction" request (transaction/transaction request))
  (POST "/confirmation" request (transaction/transaction-confirmation request))
  (route/resources "/")
  (route/not-found "Not n muahahhahahah Found"))

(def app
  (-> (handler/site app-routes)
      (middleware/wrap-json-body {:keywords? true})
       middleware/wrap-json-response))

