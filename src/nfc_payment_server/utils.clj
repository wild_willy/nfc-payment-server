(ns nfc-payment-server.utils)

(defmacro when-let*
  "When-let multiple version."
  [bindings & body]
  `(let ~bindings
     (if (and ~@(take-nth 2 bindings))
       (do ~@body)
       )))
