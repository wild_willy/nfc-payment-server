(ns nfc-payment-server.controllers.transaction
  (:require [compojure.core :refer [defroutes GET POST]]
            [ring.util.response :as ring]
            [nfc-payment-server.models.account :as account]
            [nfc-payment-server.models.transaction :as transaction]
            [nfc-payment-server.firebase :as firebase]
            [nfc-payment-server.utils :as utils]
            [nfc-payment-server.rsa :as rsa]
            [nfc-payment-server.models.card :as card]
            [nfc-payment-server.models.establishment :as establishment]))
;; {amount: 23423423
 ;;  transaction_id: kn3ejkn2kj
 ;;  establishment_name:
 ;;  }

(def transaction-id "f45kjBkw23")
(def db "postgresql://localhost:5433/payment-server")
(defn transaction
  [req]
  (let [card_id (get-in req [:body :card_id])
        estb_id (get-in req [:body :establishment])
        amount  (BigDecimal. (get-in req [:body :amount]))]
    (if (not (some nil? [card_id estb_id amount]))
      (let [card (card/get-card "postgresql://localhost:5433/payment-server" card_id)
            estab (establishment/get-establishment "postgresql://localhost:5433/payment-server" estb_id)]
        (if (not (some nil? [card estab]))
          (let [from (account/get-account "postgresql://localhost:5433/payment-server" (:account_number (first card)))
                to (account/get-account "postgresql://localhost:5433/payment-server" (:account_number (first estab)))]
            (if (> (:balance (first from)) amount)
              (let [trans_id (:id (first (transaction/create-transaction! db {:from_account (:account_number (first card))
                                                                              :to_account (:account_number (first estab))
                                                                              :amount amount})))]
                (do (firebase/send-payload firebase/client-token {:amount (str amount)
                                                           :transaction_id trans_id
                                                           :establishment_name (:name (first estab))})
                    {:status 200
                     :body {:desc "Payment Authorization Send Succesfully"}}))
              {:status 200
               :body {:desc "Insufficient Funds"}}))
          {:status 200
           :body {:desc "Invalid card_id or establishment_id"}}))
      {:status 200
       :body {:desc "Invalid json format, one or more fields are missing"}})
    )
  )

(defn transaction-confirmation
  "Checks that the transaction was succesful."
  [req]
  (let [trans-id (Integer. (get-in req [:body :transaction_id]))
        trans-status (get-in req [:body :status])]
    (if (not (some nil? [trans-id trans-status]))
      (if (= trans-status "accept")
        (let [transaction (first (transaction/get-transaction db trans-id))
              from (first (account/get-account db (:from_account transaction)))
              to (first (account/get-account db (:to_account transaction)))]
          (if (nil? transaction)
            {:status 200
             :body {:desc "Invalid transaction id."}}
            (do (account/update-balance! db (:account_number from) (- (:balance from) (:amount transaction)))
                (account/update-balance! db (:account_number to) (+ (:balance to) (:amount transaction)))
                (firebase/send-payload firebase/cashier-token {:desc "Transaction Successful"
                                                               :transaction (str trans-id)})
                {:status 200
                 :body {:desc "Transaction was Successful."}})
            ))
        {:status 200
         :body {:desc "Transaction was Cancelled."}})
      {:status 200
       :body {:desc "Invalid Json format."}})))


;; The Server knows the card (pub_key) and the establishment (pub_key),
;; where card is the client smartphone app and establishment is the sales smartphone app.
;; So card must have the pub_key (TEXT) column, and so does establishment pub_key (TEXT).
;; On creation the pub_key is send in the maps.
;; The server public key is shared to everybody.
;; When the establishment sends the payment request to the server it signs it encrypts the payload it using the
;; establishment's private key
;; Example json
;; {
;;  establishment: "establishment_id",
;;  payload: "encrypted({card_id: \"card id\",
;;                       amount: \"12.3\"})"
;; }

;; (defn transaction-request [req]
;;   (utils/when-let* [])
;;   )
