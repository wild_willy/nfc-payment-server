(ns nfc-payment-server.models.card
  (:require [clojure.java.jdbc :as sql]
            [clj-time.jdbc]
            [clj-pgp.core :as pgp]
            [clj-pgp.generate :as pgp-gen]
            [clj-pgp.keyring :as keyring]
            [clj-pgp.message :as pgp-msg]
            [clj-pgp.signature :as pgp-sig]
            [clj-time.local :as local-time]))
;; Card:
;; card_number :: varchar(19) = xxxx-xxxx-xxxx-xxxx (primary key)
;; account :: varchar(19) (foreign key)
;; created_at :: timestamp
;; state :: varchar(8) [active inactive]
;; pub_key :: text
;; postgres -D pg -p 5433

(def db "postgresql://localhost:5433/payment-server")
(defn create-cards-table
  "Creates the accounts table in the given database spec."
  [db]
  (sql/db-do-commands db (sql/create-table-ddl
                          :cards
                          [[:card_number "VARCHAR(19)" "PRIMARY KEY"]
                           [:account_number "VARCHAR(19) NOT NULL"]
                           ["FOREIGN KEY(account_number) REFERENCES accounts(account_number)" ]
                           [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                           [:state "VARCHAR(8) NOT NULL"]
                           [:pub_keyring "TEXT NOT NULL"]
                           [:priv_keyring "TEXT NOT NULL"]])))


(defn all-card
  "Retrieves all the cards from the spec database."
  [db]
  (into [] (sql/query db ["select * from cards"])))

(defn create-card!
  "Store an card in the cards table in the given database."
  [db card]
  (sql/insert! db :cards card))

(defn get-card
  "Retrieves an card by its card number from the database."
  [db card_number]
  (sql/query
   db
   ["select * from cards where card_number = ?" card_number]))

;; ;; Example of creating a card and pub key.
;; ;; Define card values.
;; (def cn "0000-0000-0000-0001")
;; (def an "0000-0000-0000-0001")
;; (def state "active")
;; ;; The EC algorithm.
;; (def ec (pgp-gen/ec-keypair-generator "secp160r2"))
;; (def rsa (pgp-gen/rsa-keypair-generator 2048))
;; (def ckp (pgp-gen/generate-keypair ec :ecdsa))

;; ;; Get public key from keypair
;; (def pubkey (pgp/public-key ckp))
;; ;; Get privatie key from keypair
;; (def privkey (pgp/private-key ckp))
;; (def keyring (pgp-gen/generate-keys
;;               cn an
;;               (master-key
;;                (keypair rsa :rsa-general)
;;                (prefer-symmetric :aes-256 :aes-128)
;;                (prefer-hash :sha512 :sha256 :sha1)
;;                (prefer-compression :zlib :bzip2))
;;               (signing-key
;;                (keypair rsa :rsa-general)
;;                (expires 36000))
;;               (encryption-key
;;                (keypair ec :ecdh))))

;; ;; Create card 
;; (def card
;;   {:card_number cn
;;    :account_number an
;;    :state state
;;    :pub_keyring (pgp/encode-ascii (:public keyring))
;;    :priv_keyring (pgp/encode-ascii (:secret keyring))})
;; ;; (create-cards-table db)
;; ;; (create-card! db card)


;; (def cn "0925542821")
;; (def an "0925542821")
;; (def state "active")
;; (def ec (pgp-gen/ec-keypair-generator "secp160r2"))
;; (def rsa (pgp-gen/rsa-keypair-generator 2048))
;; (def ckp (pgp-gen/generate-keypair ec :ecdsa))

;; (def keyring (pgp-gen/generate-keys
;;               cn an
;;               (master-key
;;                (keypair rsa :rsa-general)
;;                (prefer-symmetric :aes-256 :aes-128)
;;                (prefer-hash :sha512 :sha256 :sha1)
;;                (prefer-compression :zlib :bzip2))
;;               (signing-key
;;                (keypair rsa :rsa-general)
;;                (expires 36000))
;;               (encryption-key
;;                (keypair ec :ecdh))))

;; ;; Create card
;; (def card
;;   {:card_number cn
;;    :account_number an
;;    :state state
;;    :pub_keyring (pgp/encode-ascii (:public keyring))
;;    :priv_keyring (pgp/encode-ascii (:secret keyring))})

;; (create-card! db card)
