(ns nfc-payment-server.models.establishment
  (:require [clojure.java.jdbc :as sql]
            [clj-time.jdbc]
            [clj-pgp.core :as pgp]
            [clj-pgp.generate :as pgp-gen]
            [clj-pgp.keyring :as keyring]
            [clj-time.local :as local-time]))

(def db "postgresql://localhost:5433/payment-server")

;; Establishment:
;; name : Varchar(255)
;; establishment_id: Varchar(19) xxx-xxxxxxx-xx-xx-xx (Primary Key)
;; account_number: Varchar(19) xxxx-xxxx-xxxx-xxxx
;; created_at: timestamp

(defn create-establishments-table
  "Creates the stablisments table in the given database spec."
  [db]
  (sql/db-do-commands db (sql/create-table-ddl
                          :establishments
                          [[:establishment_id "VARCHAR(19)" "PRIMARY KEY"]
                           [:account_number "VARCHAR(19)" "NOT NULL"]
                           ["FOREIGN KEY(account_number) REFERENCES accounts(account_number)"]
                           [:pub_keyring "TEXT NOT NULL"]
                           [:priv_keyring "TEXT NOT NULL"]
                           [:name "VARCHAR(255)" "NOT NULL"]
                           [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]])))

(defn all-establishments
  "Retrieves all the establishments from the spec database."
  [db]
  (into [] (sql/query db ["select * from establishments"])))

(defn store-establishment!
  "Store an establishment in the establishment table in the given database."
  [db establishment]
  (sql/insert! db :establishments establishment))

(defn get-establishment
  "Get the establishment by the establishment id from the database."
  [db establishment_id]
  (sql/query db ["select * from establishments where establishment_id = ?" establishment_id]))

;; ;; Example Establishment
;; (def stab1 {:name "Wireless Store"
;;             :establishment_id "000-000000-00-00-00"
;;             :account_number "0000-0000-0000-0002"})


;; ;; The EC algorithm.
;; (def ec (pgp-gen/ec-keypair-generator "secp160r2"))
;; (def rsa (pgp-gen/rsa-keypair-generator 2048))
;; (def ckp (pgp-gen/generate-keypair ec :ecdsa))

;; ;; ;; Create establishment keyring
;; (def keyring (pgp-gen/generate-keys
;;               (:establishment_id stab1) (:account_number stab1)
;;               (master-key
;;                (keypair rsa :rsa-general)
;;                (prefer-symmetric :aes-256 :aes-128)
;;                (prefer-hash :sha512 :sha256 :sha1)
;;                (prefer-compression :zlib :bzip2))
;;               (signing-key
;;                (keypair rsa :rsa-general)
;;                (expires 36000))
;;               (encryption-key
;;                (keypair ec :ecdh))))

;; ;; (create-establishments-table db)
;; (def stab {:priv_keyring (pgp/encode-ascii (:secret keyring))
;;            :pub_keyring (pgp/encode-ascii (:public keyring))
;;            :name (:name stab1)
;;            :establishment_id (:establishment_id stab1)
;;            :account_number (:account_number stab1)
;;            })

;; (store-establishment! db stab)
;; Example Establishment
;; (def stab1 {:name "Wireless Cool Store"
;;             :establishment_id "46"
;;             :account_number "1717179673"})


;; ;; The EC algorithm.
;; (def ec (pgp-gen/ec-keypair-generator "secp160r2"))
;; (def rsa (pgp-gen/rsa-keypair-generator 2048))
;; (def ckp (pgp-gen/generate-keypair ec :ecdsa))

;; ;; ;; Create establishment keyring
;; (def keyring (pgp-gen/generate-keys
;;               (:establishment_id stab1) (:account_number stab1)
;;               (master-key
;;                (keypair rsa :rsa-general)
;;                (prefer-symmetric :aes-256 :aes-128)
;;                (prefer-hash :sha512 :sha256 :sha1)
;;                (prefer-compression :zlib :bzip2))
;;               (signing-key
;;                (keypair rsa :rsa-general)
;;                (expires 36000))
;;               (encryption-key
;;                (keypair ec :ecdh))))

;; ;; (create-establishments-table db)
;; (def stab {:priv_keyring (pgp/encode-ascii (:secret keyring))
;;            :pub_keyring (pgp/encode-ascii (:public keyring))
;;            :name (:name stab1)
;;            :establishment_id (:establishment_id stab1)
;;            :account_number (:account_number stab1)
;;            })

;; (store-establishment! db stab)
