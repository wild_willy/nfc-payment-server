(ns nfc-payment-server.models.account
  (:require [clojure.java.jdbc :as sql]
            [clj-time.jdbc]
            [clj-time.local :as local-time]))
(def db "postgresql://localhost:5433/payment-server")
;; Account :
;; account_number :: varchar(19) = xxxx-xxxx-xxxx-xxxx (Primary Key)
;; holder_name :: varchar(255) = William Arellano
;; balance :: numeric(15, 4) = xxxxxxxxxxx.xxxx
;; created_at :: timestamp
;; updated_at :: timestamp
(defn create-accounts-table
  "Creates the accounts table in the given database spec."
  [db]
  (sql/db-do-commands db (sql/create-table-ddl
                          :accounts
                          [[:account_number "VARCHAR(19)" "PRIMARY KEY"]
                          [:holder_name "VARCHAR(255)" "NOT NULL"]
                          [:balance "NUMERIC(15, 4)" "NOT NULL"]
                          [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                          [:last_updated_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]])))

(defn all-accounts
  "Retrieves all the accounts from the spec database."
  [db]
  (into [] (sql/query db ["select * from accounts"])))

(defn create-account!
  "Store an account in the accounts table in the given database."
  [db account]
  (sql/insert! db :accounts account))

(defn get-account
  "Retrieves an account by its an account number from the database."
  [db account_number]
  (sql/query
   db
   ["select * from accounts where account_number = ?" account_number]))

(defn update-balance!
  "Update the given account's balance."
  [db account_number balance]
  (sql/update!
   db
   :accounts
   {:balance balance
    :last_updated_at (local-time/local-now)}
   ["account_number = ?" account_number]))


;; ;; Example Accounts
;; (def account {:account_number "0000-0000-0000-0000"
;;               :holder_name "William Arellano"
;;               :balance 1000.45})
;; (def account2 {:account_number "0000-0000-0000-0002"
;;                :holder_name "Paul Silva"
;;                :balance 1000000.45})
;; (def account3 {:account_number "0925542821"
;;                :holder_name "Paul Andre Silva"
;;                :balance 1000000.45})
;; (def account4 {:account_number "1717179673"
;;                :holder_name "William Rodolfo Arellano"
;;                :balance 1000.45})
;; (create-accounts-table db)
;; (create-account! db account)
;; (create-account! db account2)
;; (create-account! db account3)
;; (create-account! db account4)

