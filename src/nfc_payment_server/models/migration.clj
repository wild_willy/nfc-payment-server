(ns nfc-payment-server.models.migration
  (:require [clojure.java.jdbc :as sql]
            [nfc-payment-server.models.card :as card]
            [nfc-payment-server.models.establishment :as establishment]
            [nfc-payment-server.models.transaction :as transaction]
            [nfc-payment-server.models.account :as account]))

(defn migrated? [db table]
  (-> (sql/query db
                 [(str "select count(*) from information_schema.tables "
                       "where table_name='" table "'")])
      first :count pos?))

(defn migrate [db table]
  (when (not (migrated? db table))
    (println (str "Creating table " table " in database..."))
    (flush)
    (account/create-accounts-table db)
    (println "DONE")))
