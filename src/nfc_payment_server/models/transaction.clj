(ns nfc-payment-server.models.transaction
  (:require [clojure.java.jdbc :as sql]))

;; Transaction
;; id: Varchar(30) (Primary Key)
;; from_account: Varchar(19)
;; to_account: Varchar(19)
;; ammount: Numeric(15, 4)
;; session_key: Varchar(15)
;; started_at: timestamp
;; billed_at: timestamp
;; finished_at: timestamp
(def db "postgresql://localhost:5433/payment-server")
(defn create-transactions-table
  "Creates the transactions table in the given database spec."
  [db]
  (sql/db-do-commands db (sql/create-table-ddl
                          :transactions
                          [[:id "SERIAL" "PRIMARY KEY"]
                           [:from_account "VARCHAR(19)"]
                           ["FOREIGN KEY(from_account) REFERENCES accounts(account_number)"]
                           [:to_account "VARCHAR(19)"]
                           ["FOREIGN KEY(to_account) REFERENCES accounts(account_number)"]
                           [:amount "NUMERIC(15, 4)"]
                           [:session_key "VARCHAR(15)"]
                           [:started_at :timestamp]
                           [:billed_at :timestamp]
                           [:finished_at :timestamp]])))

(defn all-transactions
  "Retrieves all the transactions from the spec database."
  [db]
  (into [] (sql/query db ["select * from transactions"])))

(defn create-transaction!
  "Store an account in the accounts table in the given database."
  [db transaction]
  (sql/insert! db :transactions transaction {:return-keys ["id"]}))

(defn get-transaction
  "Retrieves a transaction by its id from the database."
  [db id]
  (sql/query
   db
   ["select * from transactions where id = ?" id]))

(defn drop-transactions-table [] (sql/db-do-commands db "drop table transactions"))
;; How to save key to .asc
;; (with-open [w (io/writer "privkeyring.asc" :append true)] (.write w (str (pgp/encode-ascii (first prkr)))))
