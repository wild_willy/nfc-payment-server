(ns nfc-payment-server.firebase
  (:require [clj-http.client :as client]))

(def server-key "AAAAziBAH2g:APA91bEuEMGJYnvQrZphDW_FR8g5CwmrIBJK5QKe-OyVKJt8795Zc3gJdiyHoigxKT-Y72zCnl9kTeXXBpKzXZ1WfdRIMb1yxL5q7Z_M1Z19O64s-fKfK-U-jW_tcVYS4WR3vVLhh08B")
(def proj-id "pagare-52229")
(def firebase-url-legacy "https://fcm.googleapis.com/fcm/send")
(def firebase-url (str "https://fcm.googleapis.com/v1/projects/" (str proj-id) "/messages:send"))
(def token "e_WdPxYxbV8:APA91bGbI7dMZcDDBXqkHgM11cjRRo_Ak6GFhjk2QZVECUyRfuIzg-E_CR0pCw6tcqr1txmPgFZNyvh70RAExhyqf1c5iXzWBM45WndTI1D1hefOjikh34JyhWRwK4xjQLRTSE9UvCb3")
(def cashier-token "e5MVuEuTnBU:APA91bG4adk1yDCtq8ZoodJvXzyB0xkVFDs4hu65OereGjibQ58gtCqEXe1ufZBbOOyzLHEIw4QBdb5M_y0urXF18rAif5IvPy8W6TTqu7C_7lL96D_djDbkSy-Ea2blAo_r_vcKgSL0")
(def client-token "f1UOrr5To3w:APA91bGcMJGycR20JJQD1dfXtYfONNewxGPIvWGY9xx71r8sjlqc2l9K1Kb4JyExo4_odOnUS3S_0cD2g7vpVm7KiJrr96RKBqpHbeuHn4g2r57_WME_UuyLOgKMfOvZW5iSGKFrut7y")
;; Sample message notification
;; POST https://fcm.googleapis.com/v1/projects/myproject-b5ae1/messages:send HTTP/1.1

;; Content-Type: application/json
;; Authorization: Bearer ya29.ElqKBGN2Ri_Uz...HnS_uNreA

;; {
;;  "message":{
;;             "token" : "bk3RNwTe3H0:CI2k_HHwgIpoDKCIZvvDMExUdFQ3P1...",
;;             "notification" : {
;;                               "body" : "This is an FCM notification message!",
;;                               "title" : "FCM Message",
;;                               }
;;             }
;;  }
(defn send-notification
  "Send a payment authorization to the payment request given the application token."
  [token]
  (client/post firebase-url-legacy
               {:content-type :json
                :headers {"Authorization" (str "key=" server-key)}
                :form-params {:to token
                              :data {:body "This is send from the Clojure Server!!"
                                     :title "FCM Message Clojure"}}})
  )

(defn send-payload
  "Send a payload to the device represented by token."
  [token payload]
  (client/post firebase-url-legacy
               {:content-type :json
                :headers {"Authorization" (str "key=" server-key)}
                :form-params {:to token
                              :data payload}}))

;; Transaction Json Send From server as a notification
;; Receive from client as HTTP Post as a transaction confirmation serveraddress:port/confirmation.
;; Send
;; {amount: 23423423
;;  transaction_id: kn3ejkn2kj
;;  establishment_name:
;;  }
;; Receive
;; {status: "accept",
;;  transaction_id: 2n1ek2nd
;;  amount: 2343242342,
;;  establishment_name: ksdnfnsanf}

